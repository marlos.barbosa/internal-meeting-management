from sqlalchemy.orm import Session
from domain.models.models import Meeting
from fastapi import HTTPException

def get_meetings(db: Session):
    return db.query(Meeting).all()

def get_meeting_by_id(db: Session, meeting_id: int):
    return db.query(Meeting).filter(Meeting.id == meeting_id).first()

def create_meeting(db: Session, meeting:Meeting):
    room_availability = db.query(Meeting).filter(Meeting.room_id == meeting.room_id).filter(Meeting.start < meeting.end).filter(Meeting.end > meeting.start).all()
    if room_availability:
        raise HTTPException(status_code=400, detail="Room is already booked")
    db.add(meeting)
    db.commit()
    db.refresh(meeting)
    return meeting

def delete_meeting_by_id(db: Session, meeting_id: int):
    meeting = get_meeting_by_id(db, meeting_id)
    db.delete(meeting)
    db.commit()
    return "Meeting deleted"