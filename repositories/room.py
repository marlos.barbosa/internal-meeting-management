from sqlalchemy.orm import Session
from domain.models.models import Room

def get_rooms(db: Session):
    return db.query(Room).all()

def get_room_by_id(db: Session, room_id: int):
    return db.query(Room).filter(Room.id == room_id).first()

def create_room(db: Session, room:Room):
    room_checking = db.query(Room).filter(Room.name == room.name).first()
    if room_checking:
        return "Room name already used"
    db.add(room)
    db.commit()
    db.refresh(room)
    return room

def delete_room_by_id(db: Session, room_id: int):
    room = get_room_by_id(db, room_id)
    db.delete(room)
    db.commit()
    return "Room deleted"

def update_room(db: Session, room_id: int, new_name: str):
    room = get_room_by_id(db, room_id)
    room.name = new_name
    db.commit()
    db.refresh(room)
    return room
