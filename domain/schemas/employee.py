from domain.schemas.generic import GenericModel
from pydantic import Field, EmailStr

class EmployeeBase(GenericModel):
    name: str = Field(default="Carlos Alberto")
    email: EmailStr = Field(default="carlos.alberto@petrobreizius.com")

class EmployeeCreate(EmployeeBase):
    pass

class Employee(EmployeeBase):
    id: int = Field(default=1)

    class Config:
        from_attributes = True