from domain.schemas.generic import GenericModel
from pydantic import Field

class RoomBase(GenericModel):
    name: str = Field(default="Sala de reuniões")

class RoomCreate(RoomBase):
    pass

class Room(RoomBase):
    id: int = Field(default=1)

    class Config:
        from_attributes = True