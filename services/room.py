from sqlalchemy.orm import Session
from repositories import room as room_repository
from domain.schemas.room import RoomCreate
from domain.models.models import Room
from fastapi import HTTPException

ROOM_NOT_FOUND = "Room not found"

def get_rooms(db: Session):
    return room_repository.get_rooms(db)

def get_room_by_id(db: Session, room_id: int):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    return room_repository.get_room_by_id(db, room_id)

def create_room(db: Session, room:RoomCreate):
    room = Room(**room.dict())
    return room_repository.create_room(db, room)

def delete_room_by_id(db: Session, room_id: int):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    return room_repository.delete_room_by_id(db, room_id)

def update_room(db: Session, room_id: int, new_name: str):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    return room_repository.update_room(db, room_id, new_name)