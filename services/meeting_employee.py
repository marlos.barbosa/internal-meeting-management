from sqlalchemy.orm import Session
from repositories import meeting_employee as meeting_employee_repository
from repositories import employee as employee_repository
from repositories import meeting as meeting_repository

def add_employee_to_meeting(db: Session, meeting_id: int, employee_id: int):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        return "Employee does not exist"
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        return "Meeting does not exist"
    employee_time_availability = meeting_employee_repository.get_employee_availability(db, employee_id, meeting_id)
    if employee_time_availability:
        return "Employee is not available at this time"
    return meeting_employee_repository.add_employee_to_meeting(db, meeting_id, employee_id)

def remove_employee_from_meeting(db: Session, meeting_id: int, employee_id: int):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        return "Employee does not exist"
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        return "Meeting does not exist"
    meeting_employee = meeting_employee_repository.get_meeting_employee(db, meeting_id, employee_id)
    if meeting_employee is None:
        return "Employee is not in this meeting"
    return meeting_employee_repository.remove_employee_from_meeting(db, meeting_id, employee_id)